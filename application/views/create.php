<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- first rename this file as "create.php" and put it on views directory -->
<html>
 <head>
 <title><?php echo $title; ?></title>
 </head>
 <body>
    <form action="<?php echo $action;?>" method="post">
        <p>Username : <input type="text" name="username" value="<?php echo isset($users[0]['username']) ? $users[0]['username']: '';?>" required/></p>
        <p>Email : <input type="text" name="email" value="<?php echo isset($users[0]['email']) ? $users[0]['email']: '';?>" required/></p>
        About <textarea rows="8" name="about"><?php echo isset($users[0]['about']) ? $users[0]['about']: '';?></textarea>
      <input type="hidden" name="id" value="<?php echo isset($users[0]['id']) ? $users[0]['id']: '';?>"/>
      <p><input type="submit" name="submit" value="<?php echo $button;?>"/></p>
    </form>
 </body>
</html>