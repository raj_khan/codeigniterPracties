<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Hello_world extends CI_Controller {

    function index() {
	$data['title'] = 'Home';
	$data['message'] = 'Hello world!';
	
// load 'helloworld' view
	$this->load->view('helloworld', $data);
    }

}

?>